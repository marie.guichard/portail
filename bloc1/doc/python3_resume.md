
# Python3 en condensé

## Conventions de style d'écriture 

Toutes les conventions décrites dans cette partie et la suivante suivent celles décrites dans [PEP 8 -- Style Guide for Python Code](https://www.python.org/dev/peps/pep-0008/).

### Indentation

En Python, les structures de blocs d'instructions sont déterminées par l'*indentation*.

Utilisez quatre espaces pour chaque niveau d'indentation comme dans l'exemple ci-dessous.


```python
a = 15
while a != 1:
    if a % 2 == 0:
        a = a // 2
    else:
        a = 3 * a + 1
```

### Longueur des lignes

Évitez les lignes de code trop longues.

Une convention est de limiter les lignes à 79 caractères.

### Continuation des lignes

Il arrive qu'une expression conduise à une ligne trop longue.

* Une expression contenant une parenthèse ou une accolade ou un crochet ouvrant peut être poursuivie à la ligne suivante :


```python
print(1 + 2 + 3 +
      4 + 5 + 6 +
      7 + 8 + 9)
```

    45


* Pour de longues expressions ne commençant pas par une parentèse, accolade ou crochet ouvrant, on peut utiliser le caractère de continuation de ligne (`\`) :


```python
a = \
  1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 + 9
```

### Espaces

Votre code doit être « aéré » avec des espaces.

* affectations : `i = i + 1` <br/>
  et non : `i=i+1`
* expressions : `2 * (x + 1) / (x - 1)`<br/>
  et non : `2*(x+1)/(x-1)` 
* énumérations séparées par des virgules : `[a, b, c]`, `print(x, y)`<br/>
  et non : `[a,b,c]`, `print(x,y)`

## Conventions de nommage

### Constantes
Les constantes doivent être nommées par des identificateurs en lettres capitales, avec éventuellement des blancs soulignés ou chiffres.


```python
RACINE_DEUX = 1.414
```

### Variables

Les variables doivent être nommées par des identificateurs ne comprenant que des lettres bas de casse, avec éventuellement des blancs soulignés ou chiffres.


```python
joueur1 = 'calbuth'
joueur2 = 'timoleon'
numero_partie = 5
```

### Fonctions

Les fonctions doivent être nommées en lettres bas de casse, éventuellement avec des blancs soulignés ou chiffres.


```python
def celsius_en_fahrenheit(celsius):
    fahrenheit = 9 / 5 * celsius + 32
    return fahrenheit
```

### Mots-clés en Python3

Un *mot-clé* est un identificateur ayant un sens prédéfini dans le langage de programmation. Les mots-clés ne peuvent pas être utilisés comme identificateur de constantes, variables, fonctions, etc ..., sous peine de provoquer une ererur de syntaxe (`SyntaxError`).

Liste des mots clés du langage Python (dans la version 3).

<table>
    <tr>
        <td><tt>and</tt></td><td><tt>as</tt></td><td><tt>assert</tt></td><td><tt>break</tt></td><td><tt>class</tt></td><td><tt>continue</tt></td><td><tt>def</tt></td>
    </tr>
    <tr>
        <td><tt>del</tt></td><td><tt>elif</tt></td><td><tt>else</tt></td><td><tt>except</tt></td><td><tt>False</tt></td><td><tt>finally</tt></td><td><tt>for</tt></td>
    </tr>
    <tr>
        <td><tt>from</tt></td><td><tt>global</tt></td><td><tt>if</tt></td><td><tt>import</tt></td><td><tt>in</tt></td><td><tt>is</tt></td><td><tt>lambda</tt></td>
    </tr>
    <tr>
        <td><tt>None</tt></td><td><tt>nonlocal</tt></td><td><tt>not</tt></td><td><tt>or</tt></td><td><tt>pass</tt></td><td><tt>raise</tt></td><td><tt>return</tt></td>
    </tr>
    <tr>
        <td><tt>True</tt></td><td><tt>try</tt></td><td><tt>while</tt></td><td><tt>with</tt></td><td><tt>yield</tt></td><td><tt></tt></td><td><tt></tt></td>
    </tr>

</table>

## Commentaires

Tout ce qui suit le caractère `#` est un commentaire, et est ignoré par Python.

On distingue

* les commentaires en fin d'une ligne de code :


```python
TAUX_INTERET = 0.035  # taux d'intérêt annuel à 3.5%
```

* les lignes de commentaires :


```python
a, b, c = 1., -1., 1. # les trois coefficients d'un trinôme
delta = b * b - 4 * a * c
if delta >= 0.:
    # calcul des deux racines reelles éventuellement confondues
    x1 = (-b + sqrt(delta)) / (2 * a)
    x2 = (-b + sqrt(delta)) / (2 * a)
```

* les blocs de lignes de commentaires :


```python
# --------------------------------------------
# authors: Calbuth, Timoleon
# date: 10/07/2017
# object: example of a multiline comment
# --------------------------------------------
```

## Valeurs littérales

En programmation, on désigne par *littéral* toute suite de caractères désignant sa propre valeur.

###  Le littéral  `None`

En Python, `None` est une valeur littérale qui désigne tout ce qui n'a pas de valeur (comme la valeur d'un appel à la fonction `print` par exemple.


```python
print('Timoleon') is None
```

    Timoleon





    True



`None` est une valeur d'un type spécial qui est `NoneType`. C'est la seule valeur de ce type.


```python
type(None)
```




    NoneType



### Les entiers

Les littéraux entiers sont obtenus tout naturellement pas leur écriture habituelle en base 10.


```python
123
```




    123




```python
-123
```




    -123



On peut aussi donner des valeurs littérales dans d'autres bases que la base décimale. On préfixe alors l'écriture littérale par `0b` pour le binaire (base 2), `0o` pour l'octal (base 8) ou `0x` pour l'hexadécimal (base 16).


```python
0b1111011
```




    123




```python
0o173
```




    123




```python
-0x7B
```




    -123



En Python3, aucune limitation sur la taille des entiers (hormis la taille de la mémoire de la machine sur laquelle le programme s'exécute).

Les littéraux entiers ne contiennent jamais de point. Il ne faut pas confondre `123` qui est un entier (type int) et `123.` qui est un flottant (type float).


```python
type(123)
```




    int




```python
type(123.)
```




    float



### Les flottants

Les littéraux flottants s'écrivent uniquement en base 10. Ils comprennent tous un point dans leur écriture.


```python
123.5
```




    123.5



Ils peuvent être exprimés en notation scientifique :


```python
123e100
```




    1.23e+102



Les flottants sont limités à environ 17 chiffres significatifs, et les flottants non nuls sont, en valeur absolue, compris entre $10^{-308}$ et $10^{308}$ environ.

### Les chaînes de caractères

Les littéraux chaînes de caractères sont délimités par deux apostrophes (`'`) ou deux guillemets anglais (`"`).


```python
'Timoleon est un homme politique grec.'
```




    'Timoleon est un homme politique grec.'




```python
"Timoleon est un homme politique grec."
```




    'Timoleon est un homme politique grec.'



Avec ces délimiteurs, ils doivent être écrits sur une seule ligne.

Si nécessaire, il est possible de les écrire sur plusieurs lignes avec le caractère de continuation de ligne (`\`).


```python
'Timoleon \
est un homme \
politique grec.'
```




    'Timoleon est un homme politique grec.'



Avec un délimiteur triplé, triple apostrophe (`'''`) ou triple guillemet anglais (`"""`)
les littéraux chaînes peuvent être écrits sur plusieurs lignes. Mais dans ce cas la chaîne de caractère contient les marqueurs de fin de ligne (`\n`).

On les utilise souvent dans la documentation des fonctions (docstring).


```python
"""Timoleon
est un homme
politique grec.
"""
```




    'Timoleon\nest un homme\npolitique grec.\n'



### Les booléens

Deux valeurs littérales seulement pour les booléens : `True` et `False`. Ce sont aussi deux mots-clés du langage.

Attention à la casse ! `TRUE`, `FALSE`, `true`, ... ne sont pas des littéraux booléens.

## Opérateurs

### Opérateurs arithmétiques

<table>
<tr>
<th> Opérateur </th>
<th> Signification </th>
</tr>
<tr>
<td> <tt>+</tt> </td><td> Addition </td>
</tr>
<tr>
<td> <tt>-</tt> </td><td> Soustraction </td>
</tr>
<tr>
<td> <tt>*</tt> </td><td> Multiplication </td>
</tr>
<tr>
<td> <tt>**</tt> </td><td> Exponentiation </td>
</tr>
<tr>
<td> <tt>/</tt> </td><td> Division flottante </td>
</tr>
<tr>
<td> <tt>//</tt> </td><td> Division euclidienne : quotient </td>
</tr>
<tr>
<td> <tt>%</tt> </td><td> Division euclidienne : reste (modulo) </td>
</tr>
</table>

### Opérateurs relationnels

<table>
<tr>
<th> Opérateur </th>
<th> Signification </th>
</tr>
<tr>
<td> <tt>&lsaquo;</tt> </td><td> Inférieur </td>
</tr>
<tr>
<td> <tt>&rsaquo;</tt> </td><td> Supérieur </td>
</tr>
<tr>
<td> <tt>&lsaquo;=</tt> </td><td> Inférieur ou égal </td>
</tr>
<tr>
<td> <tt>&rsaquo;=</tt> </td><td> Supérieur ou égal </td>
</tr>
<tr>
<td> <tt>==</tt> </td><td> Égal </td>
</tr>
<tr>
<td> <tt>!=</tt> </td><td> Différent </td>
</tr>
</table>

### Opérateurs booléens

<table>
<tr>
<th> Opérateur </th>
<th> Signification </th>
</tr>
<tr>
<td> <tt>and</tt> </td><td> Et logique </td>
</tr>
<tr>
<td> <tt>or</tt> </td><td> Ou logique </td>
</tr>
<tr>
<td> <tt>not</tt> </td><td> Négation </td>
</tr>
</table>

### Précédence des opérateurs

Classement des opérateurs par priorité décroissante :

<table>
<tr><td><tt>**</tt></td></tr>
<tr><td><tt>*</tt>, <tt>/</tt>, <tt>//</tt>, <tt>%</tt></td></tr>
<tr><td><tt>+</tt>, <tt>-</tt></td></tr>
<tr><td><tt>&lsaquo;</tt>, <tt>&rsaquo;</tt>, <tt>&lsaquo;=</tt>, <tt>&rsaquo;</tt>, <tt>==</tt>, <tt>!=</tt></td></tr>
<tr><td><tt>not</tt></td></tr>
<tr><td><tt>and</tt></td></tr>
<tr><td><tt>or</tt></td></tr>
</table>

## Fonctions prédéfinies

Quelques fonctions prédéfinies en Python3 :

* `abs(x)` : renvoie la valeur absolue du nombre `x`.
* `chr(n)` : renvoie le caractère de numéro Unicode `n`.
* `cmp(x, y)` : compare les deux valeurs `x` et `y` et renvoie une valeur négative si `x < y`, zéro si `x == y` et une valeur positive si `x > y`.
* `float(arg)` : renvoie le flottant représenté par `arg` qui doit être un entier ou une chaîne de caractères.
* `format(value, format_spec)` : renvoie une chaîne de caractères dont le contenu est donné par `value` et le format par `format_spec` (voir plus loin).
* `help(arg)` : imprime une aide sur l'objet désigné par `arg`.
* `input([prompt])` : invite l'utilisateur à entrer une donnée, et renvoie cette donnée sous forme d'une chaîne de caractères.
* `int(arg)` : renvoie l'entier représenté par `arg` qui doit être un nombre ou une chaîne de caractères. 
* `len(arg)` : renvoie la longueur de `arg` qui doit être une valeur séquentielle comme une liste ou une chaîne de caractères par exemple.
* `list(arg)` : renvoie une liste dont les éléments sont ceux de `arg` qui doit être une structure de données séquentielles.
* `max(arg)` : renvoie le plus grand élément de `arg`.
* `min(arg)` : renvoie le plus petit élément de `arg`.
* `ord(c)` : renvoie le numéro Unicode du caractère `c`.
* `print(arg1, arg2, ...)` : imprime les arguments `arg1`, `arg2`, ... (voir plus loin).
* `range([start], stop, [step])` : renvoie un générateur de nombres entiers compris entre `start` (0 si `start` est omis) inclus et `stop` exclus. Si `step` est précisé, sa valeur indique l'incrément de progression dans l'énumération.
* `str(arg)` : renvoie une chaîne de caractères représentant `arg`.
* `type(arg)` : renvoie le type de `arg`.

## Entrées/sorties standards

On appelle *entrée standard* (standard input) un flux (stream) de données lues par un programme. Par défaut, l'entrée standard est lue depuis le clavier.

La fonction permettant d'effectuer une lecture de données depuis l'entrée standard est la fonction `input([prompt])`.
Cette fonction lit la donnée entrée par l'utilisateur depuis le clavier et renvoie cette donnée sous forme d'une chaîne de caractères. S'il est présent, le paramètre optionnel `[prompt]` est une chaîne de caractères qui est imprimée avant d'attendre la lecture de la donnée. 


```python
nom = input('Entrez votre nom : ')
age = input('Entrez votre âge : ')
print(nom, 'vous avez', age, 'ans.')
```

    Entrez votre nom : Timoleon
    Entrez votre âge : 35
    Timoleon vous avez 35 ans.


Dans l'exemple ci-dessus, la variable `age` a une valeur de type chaîne de caractères et non de type numérique.
Si on veut pouvoir effectuer des calculs, il faut la convertir en une valeur numérique.


```python
print(type(age))
age = float(age)
print(type(age))
print('Dans 10 ans, Timoleon aura',age + 10, 'ans.')
```

    <class 'str'>
    <class 'float'>
    Dans 10 ans, Timoleon aura 45.0 ans.


On appelle *sortie standard* (standard output) un flux de données écrites par un programme. Par défaut, la sortie standard est l'écran.

La fonction permettant d'écrire (imprimer) une ou plusieurs données sur la sortie standard est la fonction `print(arg1, arg2, ..., sep=' ', end='\n')`.
Les paramètres `arg1`, `arg2`, ... peuvent être de tout type. Le paramètre optionnel `sep` a une valeur par défaut qui est une chaîne de longueur 1 contenant une espace, cette valeur pouvant être remplacée par toute autre chaîne de caractères de longueur quelconque. Le paramètre optionnel `end` a une valeur par défaut qui est une chaîne de longueur 1 contenant le marqueur de fin de ligne, cette valeur pouvant elle aussi être remplacée par toute autre chaîne.


```python
print('Dans 10 ans, Timoleon aura',age + 10, 'ans.')
print('Dans 10 ans, Timoleon aura',age + 10, 'ans.', sep='---')
print('Dans 10 ans, Timoleon aura',age + 10, 'ans.', end='||')
print('Dans 10 ans, Timoleon aura',age + 10, 'ans.')
```

    Dans 10 ans, Timoleon aura 45.0 ans.
    Dans 10 ans, Timoleon aura---45.0---ans.
    Dans 10 ans, Timoleon aura 45.0 ans.||Dans 10 ans, Timoleon aura 45.0 ans.


## Instruction conditionnelle (`if`)

### Syntaxe

<code>
if &lsaquo;condition&rsaquo;:
   &lsaquo;bloc d'instructions&rsaquo;
elif &lsaquo;condition&rsaquo;:
   &lsaquo;bloc d'instructions&rsaquo;
...
else:
   &lsaquo;bloc d'instructions&rsaquo;
</code>

* Il peut n'y avoir aucune partie `elif` ni même `else`.
* Le bloc d'instructions exécuté est le premier pour lequel la condition est vraie. Si aucune condition n'est vraie, c'est alors le bloc d'instructions  suivant le `else` qui est exécuté s'il y a un `else`.
* Les instructions conditionnelles peuvent être imbriquées.

### Exemples

### Erreurs fréquentes

* Oubli du `:` après une condition ou le `else`.
* Mauvaise indentation, en particulier en cas d'imbrication.
* Utilisation de `=` au lieu de `==` pour le test d'égalité.

## Répétition non conditionnelle (`for`)

## Répétition conditionnelle (`while`)

## Fonctions


```python

```

## Opérations sur les séquences

Les principales structures séquentielles sont les listes, les chaînes de caractères (et les tuples).

* `len(s)` : renvoie la longueur de la séquence `s`.
* `s[i]` : renvoie l'élément se trouvant à l'indice `i` dans la séquence `s`.
* `s[i:j]` : renvoie la *tranche* (slice) des éléments de `s` d'indices compris entre `i` inclus et `j` exclus.
* `x in s` : renvoie `True` si l'élément `x` est dans `s`, et `False` dans le cas contraire.
* `s1 + s2` : renvoie une nouvelle séquence contenant les éléments de `s1` suivis de ceux de `s2` (*concaténation*).
* `s * n` ou `n * s` : renvoie une nouvelle séquence construite en répétant `n` fois la séquence `s`.

## Formatage des chaînes

## Modules

Un *module* est un fichier contenant des définitions de constantes, fonctions, ...

La bibliothèque standard de Python fournit de base un grand nombre de modules consacrés à divers thèmes.

### Espace de noms

Chaque module a son propre espace de noms. Deux modules différents peuvent définir deux objets ayant même nom.
Pour les distinguer on peut alors les désigner par un nom *pleinement qualifié* sous la forme `nom_module.nom_objet`.

### Importation

Il existe plusieurs façons d'importer des valeurs définies dans des modules

* `import nom_module` : importe toutes les définitions du module `nom_module`. Les noms doivent tous être pleinement qualifiés.
  Exemple :


```python
import math
math.sqrt(2)
```




    1.4142135623730951



* `from nom_module import nom_objet1, nom_objet2, ...` : importe uniquement les définitions nommées `nom_objet1` ... Dans ce cas les noms ne doivent pas être pleinement qualifiés.
  
  Exemple :


```python
from math import sin, pi
sin(pi)
```




    1.2246467991473532e-16



* `from nom_module import nom_objet as autre_nom` : importe uniquement la définition de nom_objet en le redéfinissant par autre_nom. Cette forme d'importation peut être utile pour éviter les conflits de noms.

  Exemple :


```python
from math import log as ln
ln(10)
```




    2.302585092994046



* `from nom_module import *` : importe toutes les définitions du module `nom_module`. Les noms ne doivent pas être pleinement qualifiés.

  Exemple :


```python
from math import *
log(e)
```




    1.0



### Module `math`

Ce module regroupe les principales constantes ($e$ et $\pi$) et fonctions usuelles en analyse mathématique.


```python
import math
dir(math)
```




    ['__doc__',
     '__loader__',
     '__name__',
     '__package__',
     '__spec__',
     'acos',
     'acosh',
     'asin',
     'asinh',
     'atan',
     'atan2',
     'atanh',
     'ceil',
     'copysign',
     'cos',
     'cosh',
     'degrees',
     'e',
     'erf',
     'erfc',
     'exp',
     'expm1',
     'fabs',
     'factorial',
     'floor',
     'fmod',
     'frexp',
     'fsum',
     'gamma',
     'hypot',
     'isfinite',
     'isinf',
     'isnan',
     'ldexp',
     'lgamma',
     'log',
     'log10',
     'log1p',
     'log2',
     'modf',
     'pi',
     'pow',
     'radians',
     'sin',
     'sinh',
     'sqrt',
     'tan',
     'tanh',
     'trunc']




```python
help(math.sin)
```

    Help on built-in function sin in module math:
    
    sin(...)
        sin(x)
        
        Return the sine of x (measured in radians).
    


### Module `random`

Ce module regroupe quelques fonctions permettant d'engendrer des données pseudo-aléatoires.


```python
import random
dir(random)
```




    ['BPF',
     'LOG4',
     'NV_MAGICCONST',
     'RECIP_BPF',
     'Random',
     'SG_MAGICCONST',
     'SystemRandom',
     'TWOPI',
     '_BuiltinMethodType',
     '_MethodType',
     '_Sequence',
     '_Set',
     '__all__',
     '__builtins__',
     '__cached__',
     '__doc__',
     '__file__',
     '__loader__',
     '__name__',
     '__package__',
     '__spec__',
     '_acos',
     '_ceil',
     '_cos',
     '_e',
     '_exp',
     '_inst',
     '_log',
     '_pi',
     '_random',
     '_sha512',
     '_sin',
     '_sqrt',
     '_test',
     '_test_generator',
     '_urandom',
     '_warn',
     'betavariate',
     'choice',
     'expovariate',
     'gammavariate',
     'gauss',
     'getrandbits',
     'getstate',
     'lognormvariate',
     'normalvariate',
     'paretovariate',
     'randint',
     'random',
     'randrange',
     'sample',
     'seed',
     'setstate',
     'shuffle',
     'triangular',
     'uniform',
     'vonmisesvariate',
     'weibullvariate']




```python
help(random.randrange)
```

    Help on method randrange in module random:
    
    randrange(start, stop=None, step=1, _int=<class 'int'>) method of random.Random instance
        Choose a random item from range(start, stop[, step]).
        
        This fixes the problem with randint() which includes the
        endpoint; in Python this is usually not what you want.
    


### Module `turtle`

Ce module offre des fonctions permettant de contrôler une tortue graphique.


```python
import turtle
dir(turtle)
```




    ['Canvas',
     'Pen',
     'RawPen',
     'RawTurtle',
     'Screen',
     'ScrolledCanvas',
     'Shape',
     'TK',
     'TNavigator',
     'TPen',
     'Tbuffer',
     'Terminator',
     'Turtle',
     'TurtleGraphicsError',
     'TurtleScreen',
     'TurtleScreenBase',
     'Vec2D',
     '_CFG',
     '_LANGUAGE',
     '_Root',
     '_Screen',
     '_TurtleImage',
     '__all__',
     '__builtins__',
     '__cached__',
     '__doc__',
     '__file__',
     '__forwardmethods',
     '__loader__',
     '__methodDict',
     '__methods',
     '__name__',
     '__package__',
     '__spec__',
     '__stringBody',
     '_alias_list',
     '_getpen',
     '_getscreen',
     '_screen_docrevise',
     '_tg_classes',
     '_tg_screen_functions',
     '_tg_turtle_functions',
     '_tg_utilities',
     '_turtle_docrevise',
     '_ver',
     'addshape',
     'back',
     'backward',
     'begin_fill',
     'begin_poly',
     'bgcolor',
     'bgpic',
     'bk',
     'bye',
     'circle',
     'clear',
     'clearscreen',
     'clearstamp',
     'clearstamps',
     'clone',
     'color',
     'colormode',
     'config_dict',
     'deepcopy',
     'defstr',
     'degrees',
     'delay',
     'distance',
     'done',
     'dot',
     'down',
     'end_fill',
     'end_poly',
     'exitonclick',
     'fd',
     'fillcolor',
     'filling',
     'forward',
     'get_poly',
     'get_shapepoly',
     'getcanvas',
     'getmethparlist',
     'getpen',
     'getscreen',
     'getshapes',
     'getturtle',
     'goto',
     'heading',
     'hideturtle',
     'home',
     'ht',
     'inspect',
     'isdown',
     'isfile',
     'isvisible',
     'join',
     'left',
     'listen',
     'lt',
     'mainloop',
     'math',
     'methodname',
     'mode',
     'numinput',
     'onclick',
     'ondrag',
     'onkey',
     'onkeypress',
     'onkeyrelease',
     'onrelease',
     'onscreenclick',
     'ontimer',
     'pd',
     'pen',
     'pencolor',
     'pendown',
     'pensize',
     'penup',
     'pl1',
     'pl2',
     'pos',
     'position',
     'pu',
     'radians',
     'read_docstrings',
     'readconfig',
     'register_shape',
     'reset',
     'resetscreen',
     'resizemode',
     'right',
     'rt',
     'screensize',
     'seth',
     'setheading',
     'setpos',
     'setposition',
     'settiltangle',
     'setundobuffer',
     'setup',
     'setworldcoordinates',
     'setx',
     'sety',
     'shape',
     'shapesize',
     'shapetransform',
     'shearfactor',
     'showturtle',
     'simpledialog',
     'speed',
     'split',
     'st',
     'stamp',
     'sys',
     'textinput',
     'tilt',
     'tiltangle',
     'time',
     'title',
     'towards',
     'tracer',
     'turtles',
     'turtlesize',
     'types',
     'undo',
     'undobufferentries',
     'up',
     'update',
     'width',
     'window_height',
     'window_width',
     'write',
     'write_docstringdict',
     'xcor',
     'ycor']




```python
help(turtle.forward)
```

    Help on function forward in module turtle:
    
    forward(distance)
        Move the turtle forward by the specified distance.
        
        Aliases: forward | fd
        
        Argument:
        distance -- a number (integer or float)
        
        Move the turtle forward by the specified distance, in the direction
        the turtle is headed.
        
        Example:
        >>> position()
        (0.00, 0.00)
        >>> forward(25)
        >>> position()
        (25.00,0.00)
        >>> forward(-75)
        >>> position()
        (-50.00,0.00)
    


## Exceptions

* `SyntaxError: invalid syntax` : 

C'est le message d'erreur le plus fréquent (surtout chez les débutants en Python). Il s'agit d'une erreur provoquée par une syntaxe incorrecte. 


```python
if 1 = 2:
    print('impossible')
```


      File "<ipython-input-38-0059bbcccdc5>", line 1
        if 1 = 2:
             ^
    SyntaxError: invalid syntax



* `SyntaxError: EOL while scanning string literal` : 

C'est encore une erreur de syntaxe. Ce message apparaît lorsqu'une chaîne de caractères est mal fermé (délimiteur de fin absent).


```python
print("hello)
```


      File "<ipython-input-39-536b3438f15d>", line 1
        print("hello)
                     ^
    SyntaxError: EOL while scanning string literal



* `IndentationError: expected an indented block` : 

Cette erreur survient lorsqu'un bloc qui devrait être indenté ne l'est pas.


```python
if 0 == 0:
print('Cette ligne devrait être indentée')
```


      File "<ipython-input-40-4f16b85f385e>", line 2
        print('Cette ligne devrait être indentée')
            ^
    IndentationError: expected an indented block



* `IndentationError: unexpected indent` : Cette fois c'est une indentation qui ne devrait pas exister.


```python
a = 0
  print(a)
```


      File "<ipython-input-41-3f437f083af1>", line 2
        print(a)
        ^
    IndentationError: unexpected indent



* `IndentationError: unindent does not match any outer indentation level` : 

C'est un message qui apparaît pour une raison inverse à la précédente, c'est-à-dire lorsqu'un bloc est mal désindenté.


```python
if 0 == 0:
    a = 1
  print("L'indentation de cette ligne ne correspond à rien.")
```


      File "<ipython-input-42-4f99d4c9df2e>", line 3
        print("L'indentation de cette ligne ne correspond à rien.")
                                                                   ^
    IndentationError: unindent does not match any outer indentation level



* `ZeroDivisionError: ...` : Erreur déclenchée lorsqu'on tente de faire une division par 0.


```python
1 // 0
```


    ---------------------------------------------------------------------------

    ZeroDivisionError                         Traceback (most recent call last)

    <ipython-input-43-8ba90f639c23> in <module>()
    ----> 1 1 // 0
    

    ZeroDivisionError: integer division or modulo by zero



```python
1 / 0
```


    ---------------------------------------------------------------------------

    ZeroDivisionError                         Traceback (most recent call last)

    <ipython-input-44-b710d87c980c> in <module>()
    ----> 1 1 / 0
    

    ZeroDivisionError: division by zero


* `NameError: name ... is not defined` : 

Cette erreur est déclenchée dès qu'une expression contient un nom qui ne correspond à aucune variable ou fonction préalablement définie.


```python
print(variable_non_definie)
```


    ---------------------------------------------------------------------------

    NameError                                 Traceback (most recent call last)

    <ipython-input-45-21f8751d5dc9> in <module>()
    ----> 1 print(variable_non_definie)
    

    NameError: name 'variable_non_definie' is not defined


* `TypeError: unsupported operand type(s) for ...` : 

Message qui signale un opérande de mauvais type.


```python
12 + "timoleon"
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-46-136916fd0aa5> in <module>()
    ----> 1 12 + "timoleon"
    

    TypeError: unsupported operand type(s) for +: 'int' and 'str'


* `TypeError: bad operand type for ...` : 

Message qui apparaît lorsqu'on passe à une fonction (ou une méthode) un argument d'un mauvais type.


```python
abs("timoleon")
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-47-2f383b31c448> in <module>()
    ----> 1 abs("timoleon")
    

    TypeError: bad operand type for abs(): 'str'


* `TypeError: ...() takes exactly ... argument (... given)` :

Message affiché lors d'un appel à une fonction avec un nombre d'arguments qui ne correspond pas à ce qui devrait être.


```python
abs(12, 3)
```


    ---------------------------------------------------------------------------

    TypeError                                 Traceback (most recent call last)

    <ipython-input-48-7270fd8561a5> in <module>()
    ----> 1 abs(12, 3)
    

    TypeError: abs() takes exactly one argument (2 given)


* `IndexError: ... index out of range` : 

Cette erreur souligne une tentative d'accès à un élément d'indice trop grand ou trop petit dans une structure séquentielle.


```python
s = "Timoleon"
s[15]
```


    ---------------------------------------------------------------------------

    IndexError                                Traceback (most recent call last)

    <ipython-input-49-9c007d4a23a5> in <module>()
          1 s = "Timoleon"
    ----> 2 s[15]
    

    IndexError: string index out of range



```python
l = [1, 2, 3]
l[-5]
```


    ---------------------------------------------------------------------------

    IndexError                                Traceback (most recent call last)

    <ipython-input-50-ea7cbe3dc40c> in <module>()
          1 l = [1, 2, 3]
    ----> 2 l[-5]
    

    IndexError: list index out of range


* `AssertionError: ...` :

Erreur déclenchée par l'instruction `assert` lorsque l'expression booléenne qu'on lui transmet prend la valeur `False`.


```python
x = 0
assert x != 0, 'x doit être non nul'
```


    ---------------------------------------------------------------------------

    AssertionError                            Traceback (most recent call last)

    <ipython-input-51-a789fb6058d1> in <module>()
          1 x = 0
    ----> 2 assert x != 0, 'x doit être non nul'
    

    AssertionError: x doit être non nul

